# file structure
    ├── aoc.png : aoc curve generated using model_eval.py
    ├── CONSTANTS.py : global constants in the project
    ├── dataset
    ├── EDA.ipynb : exploratory data analysis and training model
    ├── LICENSE
    ├── model_eval.py : performance evaluation of model on validation set 
    ├── model.py : model class + dataset class
    ├── pdm.lock : dependency management, pdm
    ├── pyproject.toml : dependency management, pdm
    ├── README.md
    ├── train.py : functions to execute one epoch of train and validate  
    └── weights
    dataset/
    ├── Arya_DataScientist_Assignment.zip : dataset provided
    ├── test_set.csv : testing without ground truth
    └── training_set.csv : training + validation set
    weights/
    ├── model.pt : trained model
    ├── model_result.npy : training loss + validation loss 
    ├── X_V.pt : selected features and rows as validation dataset
    └── Y_V.pt : ground truth for validation dataset 

# system setup
    # system info : Linux chshbh 5.10.0-14-amd64 #1 SMP Debian 5.10.113-1 (2022-04-29) x86_64 GNU/Linux
    # update system
    sudo apt update && sudo apt -y full-upgrade
    # [install pip](https://pip.pypa.io/en/stable/installation/)
    sudo apt install python3-pip
    sudo -H pip3 install --upgrade pip
    pip --version
    # [install pipx](https://pypa.github.io/pipx/installation/)
    sudo apt install pipx
    or  python3 -m pip install --user pipx
        python3 -m pipx ensurepath
    # [install pdm](https://pdm.fming.dev/)
    pipx install pdm
    
# dependency management
    pdm init -vv
    pdm install && pdm sync
    note : requirement.txt and setup.py are generated using pdm, using it on other platforms may cause unexpected result