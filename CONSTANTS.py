from typing import Final

WEIGHTS: Final[str] = "weights/"
DROPOUT_RATE: Final[float] = 0.25
FEATURES: Final[int] = 11
EPOCHS: Final[int] = 1000
DATASET: Final[str] = "dataset/"
BATCH_SIZE: Final[int] = 32
LEARNING_RATE: Final[float] = 0.00146
