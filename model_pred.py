import torch
import torchmetrics as T
import numpy as np
import warnings
import seaborn as sns
import matplotlib.pyplot as plt
import pandas as pd
from model import Classifier
from CONSTANTS import *

# silence warnings
warnings.filterwarnings("ignore", category=DeprecationWarning)
warnings.filterwarnings("ignore", category=UserWarning)

# gpu(cuda) support
use_cuda = torch.cuda.is_available()
device = torch.device("cuda:0" if use_cuda else "cpu")
torch.backends.cudnn.benchmark = True

# load model
path = "model.pt"
model = Classifier(FEATURES)
model.load_state_dict(torch.load(WEIGHTS+"model.pt"))
model.eval()

# load features
idx = np.load(WEIGHTS+"features.npy", allow_pickle=True)
# load dataset
k = pd.read_csv(DATASET+"test_set.csv")
pred = torch.nn.Sigmoid()(model(torch.tensor(k[idx].to_numpy(), dtype=torch.float)))
Y = torch.tensor(pred > 0.5, dtype=torch.int).detach().numpy()
k["Y"] = Y
k.to_csv("testing_w_prediction.csv", encoding='utf-8', index=False)