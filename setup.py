
# -*- coding: utf-8 -*-
from setuptools import setup

long_description = None
INSTALL_REQUIRES = [
    'torch>=1.11.0',
    'numpy>=1.22.4',
    'seaborn>=0.11.2',
    'pandas>=1.4.2',
    'ipython>=8.4.0',
    'tdqm>=0.0.1',
    'torchmetrics>=0.8.2',
    'jupyterlab>=3.4.2',
    'dask>=2022.5.2',
    'pandas-profiling[notebook]>=3.2.0',
]

setup_kwargs = {
    'name': '',
    'version': '',
    'description': '',
    'long_description': long_description,
    'license': 'MIT',
    'author': '',
    'author_email': ' <>',
    'maintainer': None,
    'maintainer_email': None,
    'url': '',
    'package_data': {'': ['*']},
    'install_requires': INSTALL_REQUIRES,
    'python_requires': '>=3.9',

}


setup(**setup_kwargs)
