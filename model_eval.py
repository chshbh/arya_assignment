import torch
import torchmetrics as T
import numpy as np
import warnings
import seaborn as sns
import matplotlib.pyplot as plt
import pandas as pd
from model import Classifier
from CONSTANTS import *

# silence warnings
warnings.filterwarnings("ignore", category=DeprecationWarning)
warnings.filterwarnings("ignore", category=UserWarning)

# gpu(cuda) support
use_cuda = torch.cuda.is_available()
device = torch.device("cuda:0" if use_cuda else "cpu")
torch.backends.cudnn.benchmark = True

# load model
path = "model.pt"
model = Classifier(FEATURES)
model.load_state_dict(torch.load(WEIGHTS+"model.pt"))
model.eval()

kek = np.load(WEIGHTS+"model_result.npy")
# load dataset
X = torch.load(WEIGHTS+"X_V.pt")
Y = torch.load(WEIGHTS+"Y_V.pt")

pred = torch.nn.Sigmoid()(model(X).detach())
truth = Y
t = truth.type(torch.LongTensor)
accuracy = T.Accuracy()(pred, t).item() * 100
f1_score = T.F1Score()(pred, t).item()
auroc = T.AUROC()(pred, t).item() * 100
fpr, tpr, thresholds = T.ROC()(pred, t)
print(f'Matthews correlation coefficient is {T.MatthewsCorrCoef(2)(pred,t)}')

def annotate(data, **kws):
    n = len(data)
    ax = plt.gca()
    ax.text(
        0.8,
        0.2,
        f"Accuracy {accuracy:.3f}%\n"
        f"AUROC    {auroc:.3f}%\n"
        f"F1 Score    {f1_score:.3f}\n",
        transform=ax.transAxes,
    )


df = pd.DataFrame({"TPR": tpr, "FPR": fpr})
g = sns.FacetGrid(
    data=df,
    height=5,
    aspect=1.5,
)
g.map_dataframe(sns.lineplot, x=df.columns[1], y=df.columns[0])
g.set_axis_labels(
    "False Positive Rate\n1 - Specificity", "True Positive Rate\nSensitivity"
)
g.map_dataframe(annotate)
i = [_ / 10 for _ in range(0, 11)]
g.map(sns.lineplot, x=i, y=i, alpha=0.1, color="black")
# g.set(xlim=(0,1))
# g.set(ylim=(0,1))
g.tight_layout()
g.savefig("aoc.png")

k = pd.DataFrame(kek.T, columns=["Training Loss", "Validation Loss"])
k = k.melt(var_name="Type", value_name="Loss", ignore_index=False)
k["Epochs"] = k.index
k.reset_index(inplace=True)
gg = sns.lineplot(x="Epochs", y="Loss", data=k, hue="Type")
gg.get_figure().savefig("loss.png")