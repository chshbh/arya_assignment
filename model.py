import torch
from CONSTANTS import *

class Classifier(torch.nn.Module):
    """
    binary classfier
    """

    def __init__(self, input_features: int, output: int = 1, p_dropout: float = DROPOUT_RATE):
        super().__init__()
        self.network = torch.nn.Sequential(
            torch.nn.LazyLinear(128),
            torch.nn.LeakyReLU(),
            torch.nn.Dropout(p_dropout),
            torch.nn.LazyLinear(128),
            torch.nn.LeakyReLU(),
            torch.nn.Dropout(p_dropout),
            torch.nn.LazyLinear(128),
            torch.nn.LeakyReLU(),
            torch.nn.Dropout(p_dropout),
            torch.nn.LazyLinear(output),
        )

    def forward(self, input_: torch.tensor):
        return self.network(input_)


class Dataset(torch.utils.data.Dataset):
    """The Dataset is responsible for accessing and processing single instances of data"""

    def __init__(self, features: torch.tensor, labels: torch.tensor):
        self.features = features
        self.labels = labels

    def __len__(self):
        return self.labels.size(0)

    def __getitem__(self, idx: int):
        return self.features[idx, :], self.labels[idx, :]