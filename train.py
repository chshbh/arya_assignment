import torch
from model import Classifier
from CONSTANTS import *

def train_loop(
    model: Classifier,
    dataloader: torch.utils.data.dataloader.DataLoader,
    loss_fn: torch.nn.modules.loss.BCEWithLogitsLoss,
    optimizer: torch.optim.Adam,
):
    """
    Below, we have a function that performs one training epoch.
    It enumerates data from the DataLoader,
    and on each pass of the loop does the following:
    1. Gets a batch of training data from the DataLoader
    2. Zeros the optimizer’s gradients
    3. Performs an inference - that is, gets predictions from the model for an input batch
    4. Calculates the loss for that set of predictions vs. the labels on the dataset
    5. Calculates the backward gradients over the learning weights
    6. Tells the optimizer to perform one learning step - that is,
        adjust the model’s learning weights based on the observed gradients for this batch,
        according to the optimization algorithm we chose
    7. It reports on the loss for every 100 batches.
    """
    model.train()
    size = len(dataloader.dataset)
    for batch, (X, y) in enumerate(dataloader):
        # Zero your gradients for every batch!
        optimizer.zero_grad()
        # Compute prediction and loss
        pred = model(X)
        loss = loss_fn(pred, y)
        # Backpropagation
        loss.backward()
        # Adjust learning weights
        optimizer.step()
        if batch % 64 == 0:
            loss, current = loss.item(), batch * len(X)
            # print(f"loss: {loss:>7f}  [{current:>5d}/{size:>5d}]")
    return loss.item()


def validation_loop(
    model: Classifier,
    dataloader: torch.utils.data.dataloader.DataLoader,
    loss_fn: torch.nn.modules.loss.BCEWithLogitsLoss,
):
    model.eval()
    size = len(dataloader.dataset)
    num_batches = len(dataloader)
    loss = 0
    with torch.no_grad():
        for X, y in dataloader:
            pred = model(X)
            loss += loss_fn(pred, y).item()

    correct = (
        (
            (torch.nn.Sigmoid()(model(dataloader.dataset.features)) > 0.5)
            == dataloader.dataset.labels
        ).sum()
    ).item()
    loss /= num_batches
    correct = correct * 100 / size
    # print(f"Test Error: \n Accuracy: {(correct):>0.1f}%, Avg loss: {loss:>8f} \n")
    return loss_fn(pred, y).item()